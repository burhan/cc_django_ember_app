import Ember from 'ember';
import Base from 'ember-simple-auth/authenticators/base';
import config from '../config/environment';


function isSecureUrl(url) {
  var link  = document.createElement('a');
  link.href = url;
  link.href = link.href;
  return link.protocol === 'https:';
}

export default Base.extend({

  init() {
    var globalConfig = config['ember-simple-auth'] || {};
    this.serverAuthEndpoint = globalConfig.serverAuthEndpoint || '/rest-auth';
  },

  authenticate(credentials) {
    return new Ember.RSVP.Promise((resolve, reject) => {
      const data = { username: credentials.identification, password: credentials.password };
      this.makeRequest(`${this.serverAuthEndpoint}/login/`, data).then((response) => {
        Ember.run(() => {
          resolve(response);
        });
      }, (xhr /*, status, error */) => {
        Ember.run(() => {
          reject(xhr.responseJSON || xhr.responseText);
        });
      });
    });
  },

  restore(data) {
    return new Ember.RSVP.Promise((resolve, reject) => {
      Ember.$.ajax({url: `${this.serverAuthEndpoint}/me/`}).then((/* response */) => {
        resolve(data);
      }, (/* xhr , status, error */) => {
        reject();
      });
    });
  },

  invalidate(/* data */) {
    function success(resolve) {
      resolve();
    }
    return new Ember.RSVP.Promise((resolve /*, reject */) => {
      this.makeRequest(`${this.serverAuthEndpoint}/logout/`, {}).then((/* response */) => {
        Ember.run(() => {
          success(resolve);
        });
      }, (/* xhr, status, error */) => {
        Ember.run(() => {
          success(resolve);
        });
      });
    });
  },

  makeRequest(url, data) {
    if (!isSecureUrl(url)) {
      Ember.Logger.warn('Credentials are transmitted via an insecure connection - use HTTPS to keep them secure.');
    }
    return Ember.$.ajax({
      url:         url,
      type:        'POST',
      data:        data,
      dataType:    'json',
      contentType: 'application/x-www-form-urlencoded'
    });
  },
});
